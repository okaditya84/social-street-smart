import pandas as pd
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer

# Load the dataset
df = pd.read_csv("news_websites.csv")

# Initialize the sentiment analyzer
sia = SentimentIntensityAnalyzer()

# Apply the sentiment analyzer to each headline and add the sentiment scores as columns
df['Sentiment'] = df['HEAD'].apply(lambda x: sia.polarity_scores(x)['compound'])

# Classify the headlines as positive, negative or neutral based on the compound score
df.loc[df['Sentiment'] > 0.05, 'Sentiment Class'] = 'Positive'
df.loc[df['Sentiment'] < -0.05, 'Sentiment Class'] = 'Negative'
df.loc[(df['Sentiment'] >= -0.05) & (df['Sentiment'] <= 0.05), 'Sentiment Class'] = 'Neutral'

# Print the first 10 rows of the dataset with the sentiment scores
print(df.head(10))
